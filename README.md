# urban-trans-admin
...

## How to run it
    npm install
    webpack
    npm start # visit http://localhost:8000

# Contributors
* [Andrey Shubich](https://github.com/shubich)
* [Eugene Ivankin](https://github.com/EugeneIvankin)