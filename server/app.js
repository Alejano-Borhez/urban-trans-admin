import express from 'express';
import path from 'path';

// Initialization of express application
const port = 8000;
const app = express();

app.use(express.static(path.join(__dirname, '../public/')));

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, '../public/index.html'));
});

const server = app.listen(port, () => {
  console.log(`Server is up and running on port ${port}`);
});
