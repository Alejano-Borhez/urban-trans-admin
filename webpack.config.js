const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

module.exports = {

  context: path.join(__dirname, 'src'),

  entry: {
    bundle: [
      'react-hot-loader/patch',
      'babel-polyfill',
      './index',
    ],
  },

  output: {
    path: path.join(__dirname, 'public'),
    publicPath: '/',
    filename: '[name].js',
  },

  resolve: {
    extensions: ['.js', '.jsx'],
  },

  module: {
    rules: [{
      test: /\.jsx?$/,
      exclude: /node_modules/,
      use: [
        'babel-loader',
        'source-map-loader',
      ],
      enforce: 'pre',
    }, {
      test: /\.css$/,
      use: [
        'style-loader',
        {
          loader: 'css-loader',
        },
      ],
    }, {
      test: /\.less?$/,
      use: [
        'style-loader',
        'css-loader',
        { loader: 'less-loader' },
      ],
    }, {
      test: /\.(ttf|eot|svg|woff|png)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
      loader: 'file-loader',
      options: {
        name: '[path][name].[ext]?[hash]',
      },
    },
    ],
  },

  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(),
    new UglifyJSPlugin(),
    new HtmlWebpackPlugin({
      template: './index.html',
      favicon: './favicon.png',
      hash: true,
    }),
  ],

  watch: true,
};
