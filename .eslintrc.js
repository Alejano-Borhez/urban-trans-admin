module.exports = {
  "extends": "airbnb",
  "parser": "babel-eslint",
  "env": {
    "browser": true,
    "node": true,
    "mocha": true,
    "jest": true,
  },
  "rules": {
    "linebreak-style": [0],
    "jsx-a11y/anchor-is-valid": [ "error", {}],
    "jsx-a11y/label-has-for": [ 2, {
        "allowChildren": true,
    }],
  }
};