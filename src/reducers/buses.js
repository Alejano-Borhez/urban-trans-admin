import * as types from '../constants/Buses';

const initialState = {
  buses: [],
  fetching: false,
  error: '',
};

const buses = (state = initialState, action) => {
  switch (action.type) {
    case types.GET_BUSES_REQUEST:
      return { ...initialState, fetching: true };
    case types.GET_BUSES_SUCCESS:
      return { ...initialState, buses: action.buses };
    case types.GET_BUSES_FAILURE:
      return { ...initialState, error: action.error };
    default:
      return state;
  }
};

export default buses;
