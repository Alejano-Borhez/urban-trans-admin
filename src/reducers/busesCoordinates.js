import * as types from '../constants/busesCoordinates';

const initialState = {
  busesCoordinates: [],
  fetching: false,
  error: '',
};

function busCoor(busesCoord) {
  console.log(busesCoord);
  return busesCoord.map(bus => ([
    bus.currentPosition.gpsLatitude,
    bus.currentPosition.gpsLongitude,
  ]));
}

const busesCoordinates = (state = initialState, action) => {
  switch (action.type) {
    case types.GET_BUSES_COORDINATES_REQUEST:
      return { ...state, fetching: true };
    case types.GET_BUSES_COORDINATES_SUCCESS:
      return { ...state, busesCoordinates: busCoor(action.busesCoordinates) };
    case types.GET_BUSES_COORDINATES_FAILURE:
      return { ...state, error: action.error };
    default:
      return state;
  }
};

export default busesCoordinates;
