import * as types from '../constants/Routes';

// import json from '../data/routes.json';

const initialState = {
  // routes: json,
  routes: [],
  fetching: false,
  error: '',
};

const routes = (state = initialState, action) => {
  switch (action.type) {
    case types.GET_ROUTES_REQUEST:
      return { ...initialState, fetching: true };
    case types.GET_ROUTES_SUCCESS:
      return { ...initialState, routes: action.routes };
    case types.GET_ROUTES_FAILURE:
      return { ...initialState, error: action.error };
    default:
      return state;
  }
};

export default routes;
