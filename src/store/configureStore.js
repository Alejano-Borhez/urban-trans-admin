import { createStore, combineReducers, applyMiddleware } from 'redux';
import createSagaMiddleware, { END } from 'redux-saga';

// import reducers
import routes from '../reducers/routes';
import buses from '../reducers/buses';
import busesCoordinates from '../reducers/busesCoordinates';
// ...

import rootSaga from '../sagas/rootSaga';

const sagaMiddleware = createSagaMiddleware();

const urbanTransApp = combineReducers({
  // put reducers here
  busesCoordinates,
  routes,
  buses,
});

export default (/* initialState */) => {
  const store = createStore(
    urbanTransApp,
    // initialState,
    applyMiddleware(sagaMiddleware),
  );

  store.runSaga = sagaMiddleware.run;
  store.close = () => store.dispatch(END);
  sagaMiddleware.run(rootSaga);

  return store;
};
