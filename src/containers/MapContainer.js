import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Map from '../components/Map';
import { getBusesCoordinatesAsync } from '../actions/getBusesCoordinates';

const mapStateToProps = state => ({
  ...state.busesCoordinates,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  getBusesCoordinatesAsync,
}, dispatch);

const MapContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(Map);

export default MapContainer;

