import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import RouteList from '../components/RouteList';
import { getRoutesAsync } from '../actions/getRoutes';

const mapStateToProps = state => ({
  ...state.routes,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  getRoutesAsync,
}, dispatch);

const RouteListContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(RouteList);

export default RouteListContainer;
