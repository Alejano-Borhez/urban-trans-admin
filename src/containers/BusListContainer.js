import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import BusList from '../components/BusList';
import { getBusesAsync } from '../actions/getBuses';

const mapStateToProps = (state, ownProps) => ({
  ...state.buses,
  routeIds: ownProps.match.params.id,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  getBusesAsync,
}, dispatch);

const BusListContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(BusList);

export default BusListContainer;
