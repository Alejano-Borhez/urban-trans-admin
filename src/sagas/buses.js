import { put, takeEvery, call } from 'redux-saga/effects';
import * as actions from '../actions/getBuses';
import * as types from '../constants/Buses';
import * as Api from '../lib/api';

export function* getBuses(action) {
  yield put(actions.getBusesRequest());
  try {
    const json = yield call(Api.requests.busList, action.id);
    yield put(actions.getBusesSuccess(json));
  } catch (e) {
    yield put(actions.getBusesFailure(e.message));
  }
}

export default function* watchBusesAsync() {
  yield takeEvery(types.GET_BUSES_ASYNC, getBuses);
}
