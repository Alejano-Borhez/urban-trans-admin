import { put, takeEvery, call } from 'redux-saga/effects';
import * as actions from '../actions/getBusesCoordinates';
import * as types from '../constants/busesCoordinates';
import * as Api from '../lib/api';

export function* getBusesCoordinates(action) {
  yield put(actions.getBusesCoordinatesRequest());
  try {
    const json = yield call(Api.requests.busCoordinatesList, action.payload);
    yield put(actions.getBusesCoordinatesSuccess(json));
  } catch (e) {
    yield put(actions.getBusesCoordinatesFailure(e.message));
  }
}

export default function* watchBusesCoordinatesAsync() {
  yield takeEvery(types.GET_BUSES_COORDINATES_ASYNC, getBusesCoordinates);
}
