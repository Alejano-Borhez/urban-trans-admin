import { put, takeEvery, call } from 'redux-saga/effects';
import * as actions from '../actions/getRoutes';
import * as types from '../constants/Routes';
import * as Api from '../lib/api';

export function* getRoute(action) {
  yield put(actions.getRoutesRequest());
  try {
    const json = yield call(Api.requests.routeList, action.payload);
    yield put(actions.getRoutesSuccess(json));
  } catch (e) {
    yield put(actions.getRoutesFailure(e.message));
  }
}

export default function* watchRoutesAsync() {
  yield takeEvery(types.GET_ROUTES_ASYNC, getRoute);
}
