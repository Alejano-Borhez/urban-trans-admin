import { all } from 'redux-saga/effects';

// import sagas
import watchRoutesAsync from './routes';
import watchBusesAsync from './buses';
import watchBusesCoordinatesAsync from './busesCoordinates';

export default function* rootSaga() {
  yield all([
    // put sagas here
    watchRoutesAsync(),
    watchBusesAsync(),
    watchBusesCoordinatesAsync(),
  ]);
}
