import { jsonToQueryString } from './utils';

const host = 'http://epbybrew0006t17:8080/bil';
// const host = '/bil';

const paths = {
  routeList: '/routeImage/all',
  busList: '/busImage/',
  busCoordinatesList: '/busImage/all',
};

export const fetchJson = url => fetch(url).then(res => res.json());

export const requests = {
  routeList: ({ limit, offset }) => {
    const queryString = jsonToQueryString({ limit, offset }); // ?limit=20&offset=0
    const url = `${host}${paths.routeList}${queryString}`;
    return fetchJson(url);
  },
  busList: (id) => {
    const url = `${host}${paths.busList}${id}`;
    return fetchJson(url);
  },
  busCoordinatesList: ({ limit, offset }) => {
    const queryString = jsonToQueryString({ limit, offset });
    const url = `${host}${paths.busCoordinatesList}${queryString}`;
    return fetchJson(url);
  },
};

// http://ecsc00a008e4.epam.com:8080/bil/busImage/all
// http://epbybrew0006t17:8080/bil/routeImage/all?limit=50&offset=0
