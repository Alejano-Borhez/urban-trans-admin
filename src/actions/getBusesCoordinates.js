import * as types from '../constants/busesCoordinates';

export const getBusesCoordinatesAsync = (limit, offset) => ({
  type: types.GET_BUSES_COORDINATES_ASYNC,
  payload: {
    limit,
    offset,
  },
});


export const getBusesCoordinatesRequest = () => ({
  type: types.GET_BUSES_COORDINATES_REQUEST,
});

export const getBusesCoordinatesSuccess = busesCoordinates => ({
  type: types.GET_BUSES_COORDINATES_SUCCESS,
  busesCoordinates,
});

export const getBusesCoordinatesFailure = error => ({
  type: types.GET_BUSES_COORDINATES_FAILURE,
  error,
});
