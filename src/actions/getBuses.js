import * as types from '../constants/Buses';

export const getBusesAsync = id => ({
  type: types.GET_BUSES_ASYNC,
  id,
});

export const getBusesRequest = id => ({
  type: types.GET_BUSES_REQUEST,
  id,
});

export const getBusesSuccess = buses => ({
  type: types.GET_BUSES_SUCCESS,
  buses,
});

export const getBusesFailure = error => ({
  type: types.GET_BUSES_FAILURE,
  error,
});
