import * as types from '../constants/Routes';

export const getRoutesAsync = (limit, offset) => ({
  type: types.GET_ROUTES_ASYNC,
  payload: {
    limit,
    offset,
  },
});

export const getRoutesRequest = id => ({
  type: types.GET_ROUTES_REQUEST,
  id,
});

export const getRoutesSuccess = routes => ({
  type: types.GET_ROUTES_SUCCESS,
  routes,
});

export const getRoutesFailure = error => ({
  type: types.GET_ROUTES_FAILURE,
  error,
});
