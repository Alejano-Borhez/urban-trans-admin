import React from 'react';
import { Route, Switch } from 'react-router-dom';

import RouteList from '../../containers/RouteListContainer';
import BusList from '../../containers/BusListContainer';
import Navigation from '../Navigation';
import MapContainer from '../../containers/MapContainer';
import './App.css';

const App = () => (
  <div className="App">
    <Navigation />
    <MapContainer />
    <Switch>
      <Route path="/route/:id">
        <Route component={BusList} />
      </Route>
      <Route path="*">
        <RouteList />
      </Route>
    </Switch>
  </div>
);

export default App;
