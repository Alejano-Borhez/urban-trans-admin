import React from 'react';
import { Link } from 'react-router-dom';
import { Table } from 'antd';

import './BusList.css';

const columns = [{
  title: 'Busplate',
  dataIndex: 'bus.busPlate',
  sorter: true,
  width: '20%',
  render: (busPlate, row) => <Link to={`#bus/${row.id}`}>{busPlate}</Link>,
}, {
  title: 'PassCount',
  dataIndex: 'userIds',
  render: userIds => userIds.length,
  sorter: true,
}, {
  title: 'Capacity',
  dataIndex: 'bus.capacity',
  sorter: true,
}, {
  title: 'Mileage',
  dataIndex: 'mileage',
  sorter: true,
}, {
  title: 'Revenue',
  dataIndex: 'revenue',
  sorter: true,
}];


export default class BusList extends React.Component {
  componentWillMount() {
    const { routeIds } = this.props;

    if (!this.props.fetching) {
      this.props.getBusesAsync(routeIds);
      console.log('fetching');
    }
  }

  handleTableChange = (pagination, filters, sorter) => {
    // const pager = { ...this.state.pagination };
    // pager.current = pagination.current;
    // this.setState({
    //   pagination: pager,
    // });
    // this.fetch({
    //   results: pagination.pageSize,
    //   page: pagination.current,
    //   sortField: sorter.field,
    //   sortOrder: sorter.order,
    //   ...filters,
    // });
  }

  render() {
    const route = this.props.buses[0] ? this.props.buses[0].route.description : '';

    return (
      <div className="BusList">
        <div className="Container">
          <h1>BusList - {route}</h1>

          <Table
            pagination={false}
            columns={columns}
            rowKey={record => record.id}
            dataSource={this.props.buses}
            onChange={this.handleTableChange}
          />
        </div>
      </div>
    );
  }
}
