import React from 'react';
import { Link } from 'react-router-dom';
import { Table } from 'antd';

import './RouteList.css';

const columns = [{
  title: 'Route',
  dataIndex: 'route.description',
  sorter: true,
  render: (description, row) => <Link to={`route/${row.route.id}`}>{description}</Link>,
  // render: name => `${name.first} ${name.last}`,
  filters: [
    { text: 'Bus', value: 'bus' },
    { text: 'Trolleybus', value: 'trolleybus' },
  ],
  width: '20%',
}, {
  title: 'Total passengers on board',
  dataIndex: 'totalPassengersOnBoard',
  sorter: true,
}, {
  title: 'Total passengers passed',
  dataIndex: 'totalPassengersPassed',
  sorter: true,
}, {
  title: 'Mileage',
  dataIndex: 'totalMileage',
  sorter: true,
}, {
  title: 'Revenue',
  dataIndex: 'totalAmount',
  sorter: true,
}];


export default class RouteList extends React.Component {
  componentWillMount() {
    const limit = 250;
    const offset = 0;

    if (!this.props.fetching) {
      this.props.getRoutesAsync(limit, offset);
      console.log('fetching');
    }
  }

  handleTableChange = (pagination, filters, sorter) => {
    // const pager = { ...this.state.pagination };
    // pager.current = pagination.current;
    // this.setState({
    //   pagination: pager,
    // });
    // this.fetch({
    //   results: pagination.pageSize,
    //   page: pagination.current,
    //   sortField: sorter.field,
    //   sortOrder: sorter.order,
    //   ...filters,
    // });
  }

  render() {
    return (
      <div className="RouteList">
        <div className="Container">
          <h1>RouteList</h1>

          <Table
            pagination={false}
            columns={columns}
            rowKey={record => record.id}
            dataSource={this.props.routes}
            onChange={this.handleTableChange}
          />
        </div>
      </div>
    );
  }
}
