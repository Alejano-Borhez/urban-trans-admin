import React from 'react';
import { Link } from 'react-router-dom';
import { Menu, Icon } from 'antd';

import './Navigation.css';

const { SubMenu } = Menu;
const MenuItemGroup = Menu.ItemGroup;


export default class Navigation extends React.Component {
  state = {
    current: 'mail',
  }
  handleClick = (e) => {
    console.log('click ', e);
    this.setState({
      current: e.key,
    });
  }

  render() {
    return (
      <nav className="Navigation">
        <div className="Container">
          <Menu
            onClick={this.handleClick}
            selectedKeys={[this.state.current]}
            mode="horizontal"
            className="Navigation-Menu"
          >
            <Menu.Item key="mail">
              <Link to="/">
                <Icon type="eye-o" />Главная
              </Link>
            </Menu.Item>

            <SubMenu title={<span><Icon type="setting" />Настройки</span>}>
              <MenuItemGroup title="Item 1">
                <Menu.Item key="setting:1">Option 1</Menu.Item>
                <Menu.Item key="setting:2">Option 2</Menu.Item>
              </MenuItemGroup>
              <MenuItemGroup title="Item 2">
                <Menu.Item key="setting:3">Option 3</Menu.Item>
                <Menu.Item key="setting:4">Option 4</Menu.Item>
              </MenuItemGroup>
            </SubMenu>
            <Menu.Item key="alipay">
              <Icon type="logout" />Выход
            </Menu.Item>
          </Menu>
        </div>
      </nav>
    );
  }
}
