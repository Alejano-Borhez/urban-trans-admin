import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Ymap from '../../services/yandexMap';

import './Map.css';


export default class Map extends Component {
  componentWillMount() {
    // const map = new Ymap();
    const limit = 50;
    const offset = 0;
    if (!this.props.fetching) {
      this.props.getBusesCoordinatesAsync(limit, offset);
    }
    // map.createNewMap(this.props.busesCoordinates);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.busesCoordinates.length) {
      console.log('перерисовать');
      const map = new Ymap();
      map.createNewMap(nextProps.busesCoordinates);
      // map.addBusesImages(nextProps.busesCoordinates);
    }
  }

  render() {
    return (
      <div id="map" className={`Map ${this.props.className}`} />
    );
  }
}

Map.propTypes = {
  className: PropTypes.string,
  busesCoordinates: PropTypes.arrayOf(PropTypes.array).isRequired,
  getBusesCoordinatesAsync: PropTypes.func.isRequired,
  fetching: PropTypes.bool.isRequired,
};

Map.defaultProps = {
  className: '',
};
