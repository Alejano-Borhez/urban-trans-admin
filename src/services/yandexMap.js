let instance = null;

export default class Ymap {
  constructor() {
    if (!instance) {
      instance = this;
    }
    this.map = {};
    return instance;
  }

  getUserCoordinats = () =>
    new Promise((resolve) => {
      ymaps.ready(() => {
        const geolocation = ymaps.geolocation;
        geolocation.get({
          provider: 'browser',
        }).then(result => resolve(result));
      });
    });

  initNewMap = (coordinats, busesCoordinates) => {
    // if coordinats == null What to do then?
    ymaps.ready(() => {
      this.map = new ymaps.Map('map', {
        center: coordinats.geoObjects.position,
        zoom: 17,
        controls: [],
      });
      coordinats.geoObjects.options.set('preset', 'islands#blueCircleIcon');
      this.map.geoObjects.add(coordinats.geoObjects);
      this.addBusesImages(busesCoordinates);
    });
  };

  createNewMap = (busesCoordinates) => {
    this.getUserCoordinats()
      .then(coordinats => this.initNewMap(coordinats, busesCoordinates));
    return this;
  };

  addBusImage = (busCoordinates) => {
    const longitude = busCoordinates[0];
    const latitude = busCoordinates[1];

    ymaps.ready(() => {
      const MyIconContentLayout = ymaps.templateLayoutFactory.createClass('<div style="color: #000; font-weight: bold;">$[properties.iconContent]</div>');
      const myMarker = new ymaps.Placemark([longitude, latitude], {
        iconContent: 'T4',
      }, {
        preset: 'islands#circleIcon',
        iconColor: 'red',
        iconContentLayout: MyIconContentLayout,
      });
      this.map.geoObjects.add(myMarker);
    });
  };

  addBusesImages = (busesCoordinates) => {
    for (let i = 0; i < busesCoordinates.length; i++) {
      this.addBusImage(busesCoordinates[i]);
    }
  };
}
